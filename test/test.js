const { Command, preventDefault, condition, getCommand, searchKey, config } = require('cmd-manage')
const cmd1 = new Command('cs1', {
   constraintMode: 'key'
})
cmd1.on(function (e) {
   console.log("cs1 按下")
})
   .bind('F2')

const cmd2 = new Command('cs2', {
   constraintMode: 'mouse'
})
cmd2.on('mousemove', function (e) {
   console.log("cs2 移动")
})
   .bind('LeftMouse')

const cmd3 = new Command('cs3', {
   constraintMode: 'wheel'
})
cmd3.on('wheel', function (e) {
   console.log("cs2 滚动")
})
   .bind('Wheel')

// preventDefault('menu', 'F3')