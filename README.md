# cmd manage

---

**cmd manage**是基于浏览器环境下的命令管理框架，实现命令、条件和快捷键的统一管理。

**cmd manage**基于模块化开发，包内置一个浏览器下的`CommonJS`环境模拟器在`test\CommonJS.js`，仅用于测试，请在开发中使用打包工具完成模块化。

==warning==：

- 该框架仍在开发中，仅为测试版本。
- The framework is still under development and is only a beta version.

---

# 指南

---

## 快速入门

1. 获取模块。

```js
const { Command } = require('cmd-manage')
```

2. 添加命令，并为其指定一个命令名。

```js
const cmd = new Command('cmd')
```

3. 为其添加侦听器。

```js
cmd.on(function() {
    console.log("cmd 触发")
})
```

4. 为其绑定快捷键。

```js
cmd.bind('Ctrl+Shift+A')
```

5. 打开网页按下`Ctrl+Shift+A`查看效果。

## 实现条件拦截

1. 引入条件模块。

```js
const { condition } = require('cmd-manage')
```

2. 设置条件。

```js
condition.ctd = false
```

3. 绑定条件

```js
cmd.addCondition("ctd")
```

4. 修改`condition.ctd`的值，查看快捷键是否有效。

更多关于条件的操作移步[条件的使用规范](#条件的使用规范)

## 绑定鼠标事件

1. 绑定快捷键。

```js
cmd.bind("Ctrl+LeftMouse")
```

2. 绑定事件。

```js
cmd.on('mousemove', function() {
	console.log("cmd 触发")
})
```

3. 打开页面，按住Ctrl+鼠标左键滑动即可触发cmd。

# 概念

---

## 快捷键的书写规范

1. 使用`+`符号来分隔键组合。
2. 单词的首字母大写，例如：
   - `Ctrl`
   - `Enter`
3. 使用大写字母来表示字母键，例如：`A`等。
4. `Ctrl`、`Shift`、`Alt`为副键，其余统称为主键。
5. 副键必须搭配一个主键使用。
6. 一个快捷键内可以配合多个副键，但只能有一个主键。
7. 组合键应始终写在主键之前，并遵循固定的顺序。
   - `Ctrl+Shift+Alt+主键`
   - `Ctrl+Shift+主键`
   - `Ctrl+Alt+主键`
   - ...
8. 使用【鼠标按键位置】+Mouse来表示鼠标按键，例如：
   - `LeftMouse`
   - `MiddleMouse`



## 鼠标事件的使用规范

1. 要定义鼠标事件，除了将主键定义为鼠标按键，还要绑定对应的鼠标事件

- mousedown
- mouseup
- mousemove

2. mousemove事件只在对应鼠标按下并同时滑动时才会触发。
3. 如果不定义鼠标模式，将默认使用`keydown`监听，但是由于主键不是键盘按键，事件将永远不会触发。



## 条件的使用规范
1. 模块[`condition`](#condition)用于储存所有的条件，有一些内置的条件可以直接使用。

| 条件        | 描述         |
| ----------- | ------------ |
| LeftMouse   | 左键是否按下 |
| MiddleMouse | 中键是否按下 |
| RightMouse  | 右键是否按下 |

2. 使用`command.addCondition(...condition)`来为命令添加条件，条件可以为字符串，也可以为函数（不推荐）。
3. 当传入字符串时，每个条件都将引用`condition`对应的属性，例如传入`"ctn"`时，每次按下快捷键都会查询`condition.ctn`的值，其为`true`时才会允许触发侦听器，这样有利于集中管理所有的条件。

```js
const { condition, addCommand } = require('cmd-manage')
const cmd = addCommand('cmd')
condition.ctn = false
cmd.addCondition('')
```

4. 当传入函数时，只有函数返回`true`时才会允许触发侦听器。

```js
cmd.addCondition(function() {
	return true
})
```


5. 当引用的属性是函数时，只有函数返回`true`时才会允许触发侦听器。

```js
condition.fnx = function() {
	return true
}
cmd.addCondition('fnx')
```

6. 当传入多个条件时，只有所有条件都返回`true`时才会允许触发侦听器。

```js
cmd.addCondition('ctn', 'fnx')
```

7. 可以在条件字符串前加上`!`来对引用结果取反。

```js
const { condition, addCommand } = require('cmd-manage')
const cmd = addCommand('cmd')
condition.ctn = false
cmd.addCondition("!ctn")
```

8. 无论是直接传入函数，还是引用了条件函数，函数的第一个参数都是事件触发时的`Event`对象，但是引用的条件函数可以在引用时传递字符串参数。例如：

```js
condition.fx = function(event, age1, age2) {
    console.log(age1, age2)
    return true
}
cmd.addCondition('fx:age1, age2')
```

9. `condition`内置了一些条件方法可供直接调用。

| 条件                                      | 描述                       |
| ----------------------------------------- | -------------------------- |
| `@(event, keyWord)` | 事件的源元素是否符合选择器 |

```js

```

关于内置条件方法的详情移步[条件方法](#条件方法)

## 模式约束

实例化`Command`对象的可选选项`constraintMode`用于进行模式约束，约束命令的事件类型。

拥有模式约束的命令具有以下行为：

1. 不允许绑定与模式不匹配的事件。
2. 不允许绑定与模式不匹配的按键。

```js
const cmd = new Command('cmd', {
	constraintMode: 'mouse',
    key: 'LeftMouse'
})
cmd.bind('Ctrl+A') // Throw 模式约束禁止的操作
```

所有可用的模式：

| 模式    | 描述         |
| ------- | ------------ |
| `key`   | 键盘按键模式 |
| `mouse` | 鼠标按键模式 |
| `wheel` | 鼠标滚轮模式 |

# API文档

---

## 模块

### `new Command(command[, option]) `

命令类

- command
  - 命令的名字，仅用于方便区分命令
  
- option
  - 选项
  
    - key
  
      快捷键字符串
  
    - preventDefault
  
      是否阻止由该事件触发的浏览器默认行为，默认为`false
      
    - constraintMode
    
      模式约束，用于约束命令的部分行为。详情移步[模式约束](#模式约束)。
      
    - disableModify
    
      禁用修改按键。`bind`方法依然正常使用。
    
      该选项会阻止命令加入列表，即没有办法通过`getCommand()`方法获取命令，借此来阻止命令被外部脚本修改。

### `config`

`config`是一个对象，内置了一些对配置项的操作。

### `condition`

`condition`是用于统一储存所有条件的对象。

更多`condition`的操作移步[`条件的使用规范`](#条件的使用规范)查看

### `preventDefault(...ages)`

阻止浏览器默认行为

传入需要阻止默认行为的按键描述

例如：

```js
preventDefault('menu', 'Ctrl+S')
```

阻止鼠标右键菜单和保存网页的行为

所有可选的值：

| 值         | 描述                           |
| ---------- | ------------------------------ |
| menu       | 鼠标右键菜单                   |
| wheel      | 鼠标滚轮（不建议）             |
| F*         | F1-F12功能键行为               |
| F(n)       | F(n)键的行为（n为1-12）        |
| 按键字符串 | 指定屏蔽一个按键的行为         |
| 副键+*     | 屏蔽一个指定副键任意主键的行为 |

实例：

```js
const {preventDefault} = require('cmd-manage')
// 阻止鼠标右键菜单
preventDefault('menu')
// 阻止弹出搜索框
preventDefault('F3')
// 阻止保存页面行为
preventDefault('Ctrl+S')
// 阻止所有Ctrl+Shift+任何主键触发的行为
preventDefault('Ctrl+Shift+*')
```



## 条件方法

### `@(event, keyWord)`

检测源元素或源元素的父级元素是否符合css选择器，可以用于限制快捷键触发的元素。

该方法会缓存css选择器的解析结果以便提升后续执行速度。

注意：如果快捷键事键盘事件，仅支持捕获`keydown/keyup`事件的元素才可能通过条件。

- keyWord

  css选择器，支持大部分css选择器语法，例如：`"input[type=checkbox]:checked"`

# 寄语

对本框架的建议可以提交至cffhidol@163.com，我偶尔会看。

