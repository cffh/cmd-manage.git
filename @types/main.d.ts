interface Mode {
   /**键盘按键按下 */
   keydown: KeyboardEvent,
   /**键盘按键抬起 */
   keyup: KeyboardEvent,
   /**鼠标按键按下 */
   mousedown: MouseEvent,
   /**鼠标按键抬起 */
   mouseup: MouseEvent,
   /**鼠标按键移动 */
   mousemove: MouseEvent,
   /**鼠标中键滚动 */
   wheel: WheelEvent,
   undefined: KeyboardEvent
}
type ConstraintMode = 'key' | 'mouse' | 'wheel'
interface ConditionFunction {
   (event: Event, ...age: any[]): any
}
interface Condition {
   [k: string]: boolean | ConditionFunction
}
interface Listener {
   (event: Event): any
}
interface InitKey {
   word: string,
   key?: string[]
}
interface InitKeySet {
   [x: string]: InitKey
}
interface CommandOption {
   key?: string,
   constraintMode?: ConstraintMode,
   preventDefault?: boolean,
   disableModify?: boolean
}
type DefaultBehavior = 'menu' | 'wheel' | 'Tab' | 'F*' | 'Ctrl+*' | 'Shift+*' | 'Alt+*' | 'Ctrl+Shift+*' | 'Ctrl+Alt+*' | 'Shift+Alt+*'
/**条件 */
export const condition: {
   [k: string]: boolean | ConditionFunction
}
export const config = {
   /**Command默认缺省配置 */
   CommandDefaultOption: {
      key: '',
      constraintMode: '',
      preventDefault: false
   },
   /**快捷键字符串分隔符 */
   shortcutKeySeparator: '+',
   /**条件字符串分隔符 */
   conditionSeparator: '&',
   /**迭代查找的顶层元素 */
   topElement: document,
}
/**命令类 */
export class Command {
   constructor(command: string, CommandOption?)
   /** * 快捷键字符串 */
   key: string
   /**条件字符串 */
   condition: string
   /**快捷键码（只读） */
   keyCode: string
   /**
    * 设置快捷键
    * @param {string} key 快捷键
    * @returns {ThisParameterType}
    */
   bind(key: string): this
   /**
    * 是否符合触发条件
    * @param {Event} event 触发事件对象
    * @returns {boolean} 是否符合条件
    */
   adopt(event: Event): boolean
   /**
    * 添加命令事件
    * @param {Listener} listener 事件侦听器
    * @returns {ThisParameterType}
    */
   on<K extends keyof Mode = 'keydown'>(listener: (event: Mode[K]) => any): this
   /**
    * 添加命令事件
    * @param {string} mode 事件模式
    * @param {Listener} listener 事件侦听器
    * @returns {ThisParameterType}
    */
   on<K extends keyof Mode = 'keydown'>(mode: K = 'keydown', listener: (event: Mode[K]) => any): this
   /**
    * 添加命令事件
    * @param {Object} listenerObject mode和侦听器的对象
    * @returns {ThisParameterType}
    */
   on<K extends keyof Mode = 'keydown'>(listenerObject: { [K]: (event: Mode[K]) => any }): this
   /**
    * 事件委托
    * @param word 选择器字符串
    */
   entrust(word: string): void
   /**
    * 移除指定侦听器
    * @param {Symbol} key 键名
    * @returns {ThisParameterType}
    */
   del<K extends keyof Mode = 'keydown'>(key: symbol, mode: K = 'keydown'): this
   /**
    * 触发命令
    * @param {Event} event 事件对象
    * @param {string} mode 模式
    * @returns {boolean} 为true表示触发成功
    */
   trigger<K extends keyof Mode = 'keydown'>(event: Mode[K], mode?: K = 'keydown'): boolean
   /**
    * 条件字符串数组
    * @returns {(string | ConditionFunction)[]} 条件数组
    */
   getConditionArray(): (string | ConditionFunction)[]
   /**
    * 添加条件
    * @param {string} condition 条件字符串
    */
   addCondition(...condition: string[]): this
   /**
    * 删除条件
    * @param {string} condition 条件字符串
    */
   delCondition(...condition: string[]): this
   /**
    * 是否存在指定条件
    * @param condition 条件字符串
    * @returns {boolean} 为true表示存在
    */
   hasCondition(condition: string): boolean
   /**
    * 阻止默认事件
    */
   preventDefault()
}
/**
 * 获取命令
 * @param {string} cmd 命令名
 * @returns {Command|undefined} 命令
 */
export function getCommand(cmd: string): Command | undefined
/**
 * 搜索按键命令
 * @param {string} key 按键字符串
 * @returns {Command[]} 命令数组（只读）
 */
export function searchKey(key: string): Command[]
/**
 * 阻止浏览器默认行为
 * @param {DefaultBehavior | string} ages 描述阻止按键的字符串列表，可多填
 */
export function preventDefault(...ages: DefaultBehavior[] | string[]): void